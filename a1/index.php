<?php 
require_once "./code.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP SC S01 Activity</title>
</head>
<body>    
    <h1>FULL ADDRESS:</h1>
    <p>Full Address: <?php echo getFullAddress("Philippines", "Davao", "Davao del Sur", "Blk 1 Lot 2"); ?></p>

    <h1>Letter-Based Grading</h1>
    <p>87 is equivalent to <?php echo getLetterGrade(87); ?></p>
    <p>94 is equivalent to <?php echo getLetterGrade(94); ?></p>
    <p>74 is equivalent to <?php echo getLetterGrade(74); ?></p>
</body>
</html>