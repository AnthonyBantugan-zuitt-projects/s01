<?php
function getFullAddress($country, $city, $province, $specificAddress ) {
    return "$specificAddress $city $province, $country";
}

function getLetterGrade($grade){
    if ($grade >= 98) {
    return "A+";
    }
    if ($grade >= 95 && $grade <= 97 ) {
    return "A";
    }
    if ($grade >= 92 && $grade <= 94 ) {
    return "A-";
    }
    if ($grade >= 89 && $grade <= 91 ) {
    return "B+";
    }
    if ($grade >= 86 && $grade <= 88 ) {
    return "B";
    }
    if ($grade >= 83 && $grade <= 85 ) {
    return "B-";
    }
    if ($grade >= 80 && $grade <= 82 ) {
    return "C+";
    }
    if ($grade >= 77 && $grade <= 79 ) {
    return "C";
    }
    if ($grade >= 75 && $grade <= 76 ) {
    return "A-";
    }
    if ($grade < 75) {
    return "D";
    }
}
