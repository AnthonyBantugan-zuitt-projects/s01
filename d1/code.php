<?php

//[SECTION] Comments

/*PHP Comments are written exactly the same as JS comments*/

/*
When writing any PHP code, always remember to start with the opening php tag:

<?php

Closing with ?> is optional when ONLY writing PHP code
*/

//echo is used to output data to the screen
//always remember to end every PHP statement with a semicolon (;)

//echo "Mabuhay World";

//[SECTION] Variables

$name = "John Smith";
$email = "johnsmith@mail.com";

//[SECTION] Constants

define('PI', 3.1416);

//[SECTION] Data Types
$state = "New York";
$country = "USA";
$address = $state . ', ' . $country; //concatenation via dot
$address2 = "$state, $country"; //concatenation via double quotes

//Integers
$age = 31;
$headcount = 26;

//Floats
$grade = 98.2;
$distance = 1342.12;

//Boolean
$hasTravelledAbroad = true;

//Null
$spouse = null;

//Arrays
$grades = array(98.7, 92.1, 90.2, 94.6);

//Objects
$gradesObj = (object)[
	'firstGrading' => 98.7,
	'secondGrading' => 92.1,
	'thirdGrading' => 90.2,
	'fourthGrading' => 94.6
];

$personObj = (object)[
	'fullName' => "John Smith",
	'isMarried' => false,
	'age' => 35,
	'address' => (object)[
		'state' => 'New York',
		'country' => "USA"
	]
];

//[SECTION] Operators

//Assignment Operator (=)
$x = 234;
$y = 123;

$isLegalAge = true;
$isRegistered = false;


//[SECTION] Functions
function getFullName($firstName, $middleInitial, $lastName) {
    return "$lastName, $firstName $middleInitial";
}

 //[SECTION] Selection Control Structures   
 
 //If-Elseif-Else Statement
function determineTyphoonIntensity($windSpeed){
        if($windSpeed < 30){
            return "Not a typhoon yet";
        }else if($windSpeed <= 61){
            return "Tropical depression detected";
        }else if($windSpeed >= 62 && $windSpeed <= 88){
            return "Tropical Storm detected";
        }else if($windSpeed >= 89 && $windSpeed <= 117){
return "Severe tropical storm detected";
        }else {
return "Typhoon detected";
        }
    }

//Ternary Oprator
function isUnderAge($age){
    return ($age < 18) ? true : false;
}


// Switch Statement
function determineUser($computerNumber) {
    switch($computerNumber) {
        case 1:
            return 'Linus Torvalds';
            break;
        case 2:
            return 'Steve Jobs';
            break;
        case 3:
            return 'Sid Meier';
            break;
        default: 
        return "The computer number $computerNumber is out of bounds.";
        break;
    }
}
