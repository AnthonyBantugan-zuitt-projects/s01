<?php
//PHP code can be included in another file by using the require_once keyword.

require_once "./code.php";
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S01</title>
</head>
<body>
	<!-- Variables can be used to output data in double quotes while single quotes will not work -->
	<h1>Variables:</h1>

	<p><?php echo "Good day $name! Your email is $email."; ?></p>
	<!-- When echoing constants, omit the $ sign -->
	<p><?php echo PI; ?></p>
	<p><?php echo $address; ?></p>
	<p><?php echo $address2; ?></p>

	<h1>Data Types:</h1>
	<!-- Normal echoing of boolean and null variables will not make them visible to the user. Too see their types instead, we can use the following: -->
	<p><?php echo gettype($spouse); ?></p>
	<!--  To see more detailed info on the variable, use var_dump. print_r can also be used for arrays. -->
	<p><?php var_dump($hasTravelledAbroad); ?></p>
	<p><?php print_r($grades); ?></p>
	<!--  Use the -> notation to access object properties -->
	<p><?php echo $gradesObj->firstGrading; ?></p>
	<p><?php echo $personObj->address->state; ?></p>
	<p><?php echo $grades[3]; ?></p>

	<h1>Operators:</h1>

	<h3>Arithmetic Operators</h3>
	<p>Sum: <?php echo $x + $y; ?></p>
	<p>Difference: <?php echo $x - $y; ?></p>
	<p>Product: <?php echo $x * $y; ?></p>
	<p>Quotient: <?php echo $x / $y; ?></p>

	<h3>Equality Operators</h3>
	<p>Loose Equality: <?php echo var_dump($x == 234); ?></p>
	<p>Loose Inequality: <?php echo var_dump($x != 123); ?></p>
	<p>Strict Equality: <?php echo var_dump($x === 123); ?></p>
	<p>Strict Inequality: <?php echo var_dump($x !== 234); ?></p>

	<h3>Greater/Lesser Operators</h3>
	<p>Is Lesser (or equal): <?php echo var_dump($x < $y); ?></p>
	<p>Is Greater (or equal): <?php echo var_dump($x >= $y); ?></p>

	<h3>Logical Operators</h3>
	<p>Are All Requirements Met: <?php var_dump($isLegalAge && $isRegistered); ?></p>
	<p>Are Some Requirements Met: <?php var_dump($isLegalAge || $isRegistered); ?></p>
	<p>Are Some Requirements Not Met: <?php var_dump(!$isLegalAge && !$isRegistered); ?></p>


    <h1>Functions:</h1>
    <p>Full Name: <?php echo getFullName('John', "J", "Smith"); ?></p>

    <h1>Selection Control Structures</h1>
	
	<h3>If-Elseif-Else</h3>
    <p><?php echo determineTyphoonIntensity(12); ?></p>

	<h3>Ternary Operator</h3>
	<p><?php var_dump(isUnderAge(78)); ?></p>
	<p><?php var_dump(isUnderAge(13)); ?></p>

	<h3>Switch Statement</h3>
	<p><?php echo determineUser(2); ?></p>
	
</body>
</html>